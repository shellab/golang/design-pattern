package main

import (
	"fmt"
	"math/rand"
	"time"
)

type MessageBoard struct {
	messageQueue chan string
}

func NewMessageBoard() *MessageBoard {
	mb := &MessageBoard{
		messageQueue: make(chan string, 10),
	}
	return mb
}

func (m *MessageBoard) SendMessage(msg string) {
	m.messageQueue <- msg
}

func (m *MessageBoard) ShowMessage() {
	for {
		fmt.Println("message:", <-m.messageQueue)
	}
}

func main() {
	messageBoard := NewMessageBoard()
	go func() {
		messageBoard.ShowMessage()
	}()

	go func() {
		n := 1
		for {
			time.Sleep(time.Duration(rand.Intn(10)) * time.Millisecond)
			messageBoard.SendMessage(fmt.Sprintf("%d: %s", n, "hello"))
			n++
		}
	}()

	go func() {
		n := 1
		for {
			time.Sleep(time.Duration(rand.Intn(10)) * time.Millisecond)
			messageBoard.SendMessage(fmt.Sprintf("%d: %s", n, "world"))
			n++
		}
	}()

	<-time.After(time.Duration(10) * time.Second)
	fmt.Println("done")
}
