package main

import (
	"fmt"
	"sync"
	"time"
)

type Counter struct {
	lock  sync.Mutex
	count int
}

func NewCounter() *Counter {
	return &Counter{}
}

func (c *Counter) Add() {
	c.lock.Lock()
	defer c.lock.Unlock()
	c.count++
}

func (c *Counter) Total() {
	fmt.Println("counter:", c.count)
}

func Count1000(c *Counter) {
	for i:=0; i<1000; i++ {
		c.Add()
	}
}

func main() {
	c := NewCounter()
	go Count1000(c)
	go Count1000(c)

	<-time.After(time.Duration(1) * time.Second)
	c.Total()
}
