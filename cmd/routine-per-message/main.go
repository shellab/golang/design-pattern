package main

import (
	"fmt"
	"sync"
	"time"
)

type NewsBoardcaster struct {
	news []string
}

func NewNewsBoardcaster(news []string) NewsBoardcaster {
	return NewsBoardcaster{
		news: news,
	}
}

func (n *NewsBoardcaster) StartPush() {
	wg := sync.WaitGroup{}
	for _, ns := range n.news {
		wg.Add(1)
		go func(ns string) {
			n.PushNews(ns)
			wg.Done()
		}(ns)
	}
	wg.Wait()
}

func (n *NewsBoardcaster) PushNews(news string) {
	time.Sleep(time.Duration(3) * time.Second)
	fmt.Println("News:", news)
}

func main() {
	news := []string{
		"helloworld",
		"how are you",
		"what do you like",
		"are you ok?",
	}
	nb := NewNewsBoardcaster(news)
	nb.StartPush()

	fmt.Println("done")
}
