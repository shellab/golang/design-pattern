package main

import (
	"fmt"
	"sync"
	"time"
)

type Counter struct {
	lock  sync.RWMutex
	count int
}

func NewCounter() *Counter {
	return &Counter{}
}

func (c *Counter) Add() { // write separate
	c.lock.Lock()
	defer c.lock.Unlock()
	c.count++
}

func (c *Counter) Show() { // read togeter
	c.lock.RLock()
	defer c.lock.RUnlock()
	fmt.Println("counter:", c.count)
}

func Count1000(c *Counter) {
	for i:=0; i<10; i++ {
		c.Add()
	}
}

func Read1000(c *Counter) {
	for i:=0; i<10; i++ {
		c.Show()
	}
}

func main() {
	c := NewCounter()
	go Count1000(c)
	go Read1000(c)
	go Count1000(c)
	go Read1000(c)

	<-time.After(time.Duration(3) * time.Second)
}
